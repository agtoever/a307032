#!/bin/env python3
"""
Generate a permutation of natural numbers.
All natural numbers are placed an a row in sorted order.
For each number the prime factorisation is calculated.
Each number is moved a number of steps towards larger numbers.
The number of steps is the sum of all prime factors of that numbers.

See: https://oeis.org/draft/A307032
"""


from sympy import factorint


def a001414(number):
    """ From https://oeis.org/A001414 """
    return sum(p*e for p, e in factorint(number).items())


def a307032(number):
    """ Generates A307032 """
    seq_max = max([m + a001414(m) for m in range(1, number + 1)]) + 1
    result = list(range(seq_max))
    for i in range(1, seq_max):
        cur_idx = result.index(i)
        steps = a001414(i)
        result.remove(i)
        if cur_idx+steps > len(result):
            result.extend([None] * (cur_idx + steps + 1 - len(result)))
            result[cur_idx+steps] = i
        else:
            result = result[:cur_idx+steps] + [i] + result[cur_idx+steps:]
    return result[number]


def main():
    """ Main program """
    print([a307032(n) for n in range(1, 80)])


if __name__ == '__main__':
    main()
